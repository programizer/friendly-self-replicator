#include <iostream>
using namespace std;



int main(int argc, char const *argv[]) {
  if(argc < 2)
  {
      printf("Needs one argument: <depth>\n");
      return 1;
  }

  int depth = stoi(argv[1]); 
  cout << "depth "<< depth << endl;
  if (depth>0)
  {
    system("mkdir -p sub-dir-for-copy"); // -p: only if it doesn't exist etc...
    system("cp -u a.out sub-dir-for-copy/a.out"); // -u: update (only if newer, so usually yes)
    string execute_command_string="cd sub-dir-for-copy && ./a.out "+to_string(depth-1);
    system(execute_command_string.c_str());
  }


  return 0;
}
