SOURCES=$(HOME)/devel/sources
vpath %.cc $(SOURCES)
vpath %.cpp $(SOURCES)
vpath %.c $(SOURCES)

CXX = g++
CXXFLAGS = -fopenmp -O3 -std=c++11 -I. -I $(SOURCES)/lib
LIBS = -lgsl -lgslcblas

PROGRAM = a.out
OBJS = source-code-for-self-copying-and-executing-binary.o



$(PROGRAM): $(OBJS)
	$(CXX) -o $@ $(CXXFLAGS) $^ $(LIBS)

clean:
	rm -f $(PROGRAM) $(OBJS)

%.o: %.cpp $(HDRS)
	$(CXX) $(CXXFLAGS) -c $<

%.o: %.cc $(HDRS)
	$(CXX) $(CXXFLAGS) -c $<

%.o: %.c $(HDRS)
	gcc $(CXXFLAGS) -c $<


debug: $(OBJS)
	$(CXX) -o $@ $(CXXFLAGS) $^ $(LIBS) -g
	gdb ./debug
	rm -f debug
