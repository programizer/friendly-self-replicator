## FriendlySelfReplicator  
  
### Description  
  
FriendlySelfReplicator is a very simple virtual "life" form in computer memory space, where self-replication is easy because you can simply ask the world to clone you.  
  

### Build  (Linux)
  
Building is simple as only one C++ source file is contained.

You can either build it with   
```
gcc source-code-for-self-copying-and-executing-binary.cc
  
``` 

or

```
make

```


### Run  

```  
./a.out <  depth  =  number of times it copies itself into an increasingly deep subdirectory  >
``` 

### Misc

Unlike the type of computer virus called "worm", FriendlySelfReplicator doesn't have the ability to copy itself over network and has no harmful payload.